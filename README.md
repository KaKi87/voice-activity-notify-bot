# voice-activity-notify-bot

Simple Discord bot to receive notifications on voice activity in a guild.

### How to use ?

Ping from a guild to subscribe or unsubscribe notifications.

DM before joining to send a custom notification message.

### How does it work ?

```
Join event
     |
Was channel empty ?
     |
YES / \
   |   (Stop)
   |
Did last event
from member
and from guild
occured earlier
than 5 mins ago ?
     |
YES / \
   |  (Stop)
   |
After 10 secs
did user stayed ?
     |
YES / \
   |  (Stop)
   |
(Notify)
```

### Can it run on multiple guilds simultaneously ?

Yes.

### Is it FOSS ?

Yes, it is released under the [MIT license](LICENSE).